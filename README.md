Django Boilerplate
===================

Django Boilerplate es una pequeña aplicación con todo lo necesario para comenzar un nuevo proyecto.


------------------------------------------------------------------------------------

Instalación
-----------
La aplicación Django Boilerplate depende de los siguientes softwares externos:

- [**PIP**](https://pypi.python.org/pypi/pip) (Probado con la versión 1.5.6)

Una vez instalados estos softwares se debe proceder a instalar las dependencias de la aplicación desde pip.

Instalación de softwares externos:
----------------------------------

**PIP:**

Instalación en OS X:

    $ sudo easy_install pip


Instalación de dependencias:
----------------------------

Las dependencias de la aplicación son instaladas a través del gestor de paquetes de python, para ello se deben ejecutar los siguientes comandos en el directorio de la aplicación:

    $ make install

------------------------------------------------------------------------------------

Configuración
-------------

Como todo proyecto en django, debe indicar las credenciales para conectase a su base de datos. Esto debe hacerse en el archivo ***config/settings.py***.

	DATABASES = {
	    'default': {
	        'ENGINE': 'django.db.backends.postgresql_psycopg2',
	        'NAME': 'django_boilerplate',
	        'USER': 'myuser',
	        'PASSWORD': 'mypassword',
	        'HOST': 'localhost',
	        'PORT': '5432',
	    }
	}

**Sincronización de base de datos:**

	$ make sync

------------------------------------------------------------------------------------

Ejecución
-----------

**Iniciar servidor en modo desarrollo:**

    $ make run

**Iniciar servidor en modo producción:**

    $ make production

Es necesario que ud. configure el target 'production' de acuerdo con su propia configuración.

------------------------------------------------------------------------------------

Otros Comandos
---------------

**Resetear la aplicación:**

    $ make flush

**Realizar backup de datos:**

    $ make backup

**Ejecutar pruebas unitarias:**

    $ make test

Es necesario que ud. configure el target 'production' de acuerdo con su propia configuración.

------------------------------------------------------------------------------------

Créditos
--------

- Danilo Aburto Vivians (danilo@requies.cl)
