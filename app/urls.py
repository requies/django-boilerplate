# -*- encoding: utf-8 -*-
from django.conf.urls import patterns, url, include
from django.conf import settings

urlpatterns = patterns('app.views',

    url(r'^login/$', 'sessions.new', name='new_session'),
    url(r'^signin/$', 'sessions.create', name='create_session'),
    url(r'^signout/$', 'sessions.destroy', name='destroy_session'),

    url(r'^categories/$', 'categories.index', name='categories'),
    url(r'^categories/new/$', 'categories.new', name='new_category'),
    url(r'^categories/create/$', 'categories.create', name='create_category'),
    url(r'^categories/(\d+)/$', 'categories.show', name='show_category'),
    url(r'^categories/(\d+)/edit/$', 'categories.edit', name='edit_category'),
    url(r'^categories/(\d+)/update/$', 'categories.update', name='update_category'),
    url(r'^categories/(\d+)/delete/$', 'categories.delete', name='delete_category'),
    url(r'^categories/(\d+)/destroy/$', 'categories.destroy', name='destroy_category'),

    url(r'^stories/$', 'stories.index', name='stories'),
    url(r'^stories/new/$', 'stories.new', name='new_story'),
    url(r'^stories/create/$', 'stories.create', name='create_story'),
    url(r'^stories/(\d+)/$', 'stories.show', name='show_story'),
    url(r'^stories/(\d+)/edit/$', 'stories.edit', name='edit_story'),
    url(r'^stories/(\d+)/update/$', 'stories.update', name='update_story'),
    url(r'^stories/(\d+)/delete/$', 'stories.delete', name='delete_story'),
    url(r'^stories/(\d+)/destroy/$', 'stories.destroy', name='destroy_story'),
)
