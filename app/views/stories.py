# -*- encoding: utf-8 -*-
# Resource:
# Story story Stories stories

from app.views import *
from app.models import Story
from app.forms import StoryForm

@login_required
def index(request):
    """
    Muestra el listado de Stories ingresadas en la aplicación.
    """
    
    # Se obtienen los objetos
    stories = Story.objects.all()

    return render_to_response(
        'stories/index.html', 
        {
            'page_title': 'Stories',
            'stories': stories
        }, 
        context_instance=RequestContext(request)
    )

@login_required
def show(request, id):
    """
    Muestra una story
    """

    story = get_object_or_404(Story, pk=id)

    return render_to_response(
        'stories/show.html', 
        {
            'page_title': 'Show story',
            'story': story
        }, 
        context_instance=RequestContext(request)
    )

@login_required
def new(request):
    """
    Envia un formulario para crear una nueva story
    """

    story_form = StoryForm(request.POST or None)

    return render_to_response(
        'stories/new.html', 
        {
            'page_title': 'New story',
            'story_form': story_form
        }, 
        context_instance=RequestContext(request)
    )

@login_required
def create(request):
    """
    Crea una nueva story
    """
    
    # Solo se permite post
    if not request.method == 'POST':
        raise Http404

    # Se obtienen los datos del formulario
    story_form = StoryForm(request.POST)

    # Si los datos no son validos se muestra nuevamente el formulario
    if not story_form.is_valid():
        return new(request)
    
    # Se guarda y redirecciona
    story_form.save()    
    return HttpResponseRedirect(reverse('stories'))

@login_required
def edit(request, id):
    """
    Envia un formulario para actualizar una nueva story
    """

    story = get_object_or_404(Story, pk=id)

    if (request.POST):
        story_form = StoryForm(request.POST)
    else:
        story_form = StoryForm(instance=story)

    return render_to_response(
        'stories/edit.html', 
        {
            'page_title': 'Edit story',
            'story': story,
            'story_form': story_form
        }, 
        context_instance=RequestContext(request)
    )

@login_required
def update(request, id):
    """
    Actualiza una nueva story
    """
    
    # Solo se permite post
    if not request.method == 'POST':
        raise Http404

    # Se obtienen los datos del formulario
    story_form = StoryForm(request.POST)

    # Si los datos no son validos se muestra nuevamente el formulario
    if not story_form.is_valid():
        return edit(request, id)
    
    # Se guarda y redirecciona
    story = story_form.save()    
    return HttpResponseRedirect(reverse('show_story', args=(story.id,)))

@login_required
def delete(request, id):
    """
    Muestra el formulario de confirmación para borrar una story
    """

    story = get_object_or_404(Story, pk=id)

    return render_to_response(
        'stories/delete.html', 
        {
            'page_title': 'Delete story',
            'story': story
        }, 
        context_instance=RequestContext(request)
    )

@login_required
def destroy(request, id):
    """
    Elimina una story
    """

    story = get_object_or_404(Story, pk=id)
    story.delete()    
    return HttpResponseRedirect(reverse('stories'))