# -*- encoding: utf-8 -*-
# Resource:
# Session session Sessions sessions

from app.views import *

# Session
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth import logout

def new(request):
    """
    Muestra el formulario de inicio de sesión
    """

    return render_to_response(
        'sessions/login.html', 
        {
            'page_title': 'Login'
        }, 
        context_instance=RequestContext(request)
    )

def create(request):
    """
    Crea una nueva session en la aplicacion
    """
    if not request.method == 'POST':
    	raise Http404

    user = authenticate(
    	username=request.POST['username'], 
    	password=request.POST['password']
    )
    if user:
        print('Autenticado')
        if user.is_active:
            print('Activo')
            login(request, user)
            return HttpResponseRedirect(reverse('home'))
        else:
            print('No activo')
            messages.warning(request, 'Your account has been deactivated')
            return HttpResponseRedirect(reverse('home'))
    else:
        print('No Autenticado')
        messages.error(request, 'Wrong username or password')
        return HttpResponseRedirect(reverse('home'))

@login_required
def destroy(request):
    """
    Elimina la session actual
    """
    logout(request)
    return HttpResponseRedirect(reverse('home'))