# -*- encoding: utf-8 -*-

from app.views import *

@login_required
def home(request):
    """
    App home
    """

    return render_to_response(
        'application/home.html', 
        {
            'page_title': 'Django Boilerplate'
        }, 
        context_instance=RequestContext(request)
    )