# -*- encoding: utf-8 -*-

# Http
from django.http import Http404
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.shortcuts import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.template import RequestContext
 
# Decorators
from django.contrib.auth.decorators import login_required
 
# Messages
from django.contrib import messages

# URL
from django.core.urlresolvers import reverse

# auth_user
from django.contrib.auth.models import User