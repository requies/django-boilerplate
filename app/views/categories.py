# -*- encoding: utf-8 -*-
# Resource:
# Category category Categories categories

from app.views import *
from app.models import Category
from app.forms import CategoryForm

@login_required
def index(request):
    """
    Muestra el listado de Categories ingresadas en la aplicación.
    """
    
    # Se obtienen los objetos
    categories = Category.objects.all()

    return render_to_response(
        'categories/index.html', 
        {
            'page_title': 'Categories',
            'categories': categories
        }, 
        context_instance=RequestContext(request)
    )

@login_required
def show(request, id):
    """
    Muestra una category
    """

    category = get_object_or_404(Category, pk=id)

    return render_to_response(
        'categories/show.html', 
        {
            'page_title': 'Show category',
            'category': category
        }, 
        context_instance=RequestContext(request)
    )

@login_required
def new(request):
    """
    Envia un formulario para crear una nueva category
    """

    category_form = CategoryForm(request.POST or None)

    return render_to_response(
        'categories/new.html', 
        {
            'page_title': 'New category',
            'category_form': category_form
        }, 
        context_instance=RequestContext(request)
    )

@login_required
def create(request):
    """
    Crea una nueva category
    """
    
    # Solo se permite post
    if not request.method == 'POST':
        raise Http404

    # Se obtienen los datos del formulario
    category_form = CategoryForm(request.POST)

    # Si los datos no son validos se muestra nuevamente el formulario
    if not category_form.is_valid():
        return new(request)
    
    # Se guarda y redirecciona
    category_form.save()    
    return HttpResponseRedirect(reverse('categories'))

@login_required
def edit(request, id):
    """
    Envia un formulario para actualizar una nueva category
    """

    category = get_object_or_404(Category, pk=id)

    if (request.POST):
        category_form = CategoryForm(request.POST)
    else:
        category_form = CategoryForm(instance=category)

    return render_to_response(
        'categories/edit.html', 
        {
            'page_title': 'Edit category',
            'category': category,
            'category_form': category_form
        }, 
        context_instance=RequestContext(request)
    )

@login_required
def update(request, id):
    """
    Actualiza una nueva category
    """
    
    # Solo se permite post
    if not request.method == 'POST':
        raise Http404

    # Se obtienen los datos del formulario
    category_form = CategoryForm(request.POST)

    # Si los datos no son validos se muestra nuevamente el formulario
    if not category_form.is_valid():
        return edit(request, id)
    
    # Se guarda y redirecciona
    category = category_form.save()    
    return HttpResponseRedirect(reverse('show_category', args=(category.id,)))

@login_required
def delete(request, id):
    """
    Muestra el formulario de confirmación para borrar una category
    """

    category = get_object_or_404(Category, pk=id)

    return render_to_response(
        'categories/delete.html', 
        {
            'page_title': 'Delete category',
            'category': category
        }, 
        context_instance=RequestContext(request)
    )

@login_required
def destroy(request, id):
    """
    Elimina una category
    """

    category = get_object_or_404(Category, pk=id)
    category.delete()    
    return HttpResponseRedirect(reverse('categories'))