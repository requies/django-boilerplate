# -*- encoding: utf-8 -*-
# Resource:
# Category category Categories categories

from django.db import models

class Category(models.Model):

    id =   models.AutoField('ID', primary_key=True)
    name = models.CharField('Name', max_length=255, null=False, blank=False)

    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        app_label = 'app'
        verbose_name_plural = 'categories'