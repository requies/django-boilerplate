# -*- encoding: utf-8 -*-
# Resource:
# Story story Stories stories

from django.db import models

class Story(models.Model):

    id =      models.AutoField('ID', primary_key=True)
    name =    models.CharField('Name', max_length=255, null=False, blank=False)
    content = models.TextField('Content', blank=True)

    # Foreign Keys
    category = models.ForeignKey('Category')

    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        app_label = 		  'app'
        verbose_name_plural = 'stories'