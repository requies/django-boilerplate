/*
 * Site controller
 */

/*
 * Un borde representa cada uno de los cuatros lados de la correa
 */

var Border = function (direction, sense, length, remainder) {

    // Dirección que deben seguir los elementos dentro de un borde
    // Horizontal o vertical
    this.direction = direction

    // Sentido que deben seguir los elementos 'ltr', 'rtl', 'ttb' y 'btt'
    this.sense = sense

    // Longitud del borde
    this.length = length

    // Longitud del borde que no ha sido ocupada
    this.remainder = remainder || this.length

}

/*
 * Se define la clase 'Correa'
 *
 * @param $container: objeto jQuery con la div donde la correa será mostrada
 * @param empalmes_json: arreglo de objetos json que definen un empalme
 * @param tramos_json: arreglo de objetos json que definen un tramo
 */
var Correa = function($container, empalmes_json, tramos_json) {

    // Objeto jQuery con el contenedor de todos los elementos de la correa
    this.$container = $container

    // Arreglo de empalmes, cada posicion alberga un objeto jQuery con
    // un empalme (elemento html)
    this.empalmes = undefined

    // Arreglo de tramos, cada posicion alberga un objeto jQuery con
    // un tramo (elemento html)
    this.tramos = undefined

    // Espesor de la correa
    this.thickness = undefined

    // Arreglo con los bordes de la correa
    this.borders = undefined

    /*
     * Variables usadas para iterar sobre tramos y empalmes
     */

    // Tipo de elemento actual, puede ser tramo o empalme
    this.current_element_type = undefined

    // Borde actual sobre el que se está iterando
    this.current_border = 0

    // Empalme actual sobre el que se está iterando
    this.current_empalme = 0

    // Tramo actual sobre el que se está iterando
    this.current_tramo = 0

    // Helper del constructor donde se realizan todos los cálculos iniciales
    this.init(empalmes_json, tramos_json)
}

Correa.prototype = {
    init: function (empalmes_json, tramos_json) {

        var $tramos = []
          , $empalmes = []
          , correa_id = this.$container.attr('data-_id')
          , $new_empalme
          , $new_tramo
          , empalme
          , tramo
          , empalme_href
          , tramo_href

        // Se insertan todos los empalmes y tramos
        for (var i = 0; i < empalmes_json.length; i++) {
            empalme = empalmes_json[i]
            tramo = tramos_json[i]

            // Se crean los objetos de tipo jQuery
            empalme_href = '/correas/' + correa_id + '/empalmes/' + empalme['_id']
            tramo_href = '/correas/' + correa_id + '/tramos/' + tramo['_id']


            $new_empalme = $('<a href="' + empalme_href + '" class="empalme"></a>').addClass('empalme-' + empalme.state)
            $new_tramo = $('<a href="' + tramo_href + '" class="tramo"></a>').addClass('tramo-' + tramo.state)

            for (key in empalme) {
                $new_empalme.attr('data-' + key, empalme[key])
            }

            for (key in tramo) {
                $new_tramo.attr('data-' + key, tramo[key])
            }

            this.$container.append($new_empalme)
            this.$container.append($new_tramo)

            $empalmes.push($new_empalme)
            $tramos.push($new_tramo)
        }

        
        this.empalmes = $empalmes
        this.tramos = $tramos

        // Se asigna el grosor de la correa dependiendo de las dimensiones
        // del empalme
        this.thickness = $new_empalme.width()

        // Los bordes pueden ser creados una vez que se conoce el espesor
        // de la correa
        this.borders = [
              new Border('horizontal', 'ltr', this.getWidth() - this.thickness)
            , new Border('vertical', 'ttb', this.getHeight() - this.thickness)
            , new Border('horizontal', 'rtl', this.getWidth() - this.thickness)
            , new Border('vertical', 'btt', this.getHeight() - this.thickness)
        ]
    }

    , getWidth: function () {
        return this.$container.width()
    }

    , getHeight: function () {
        return this.$container.height()
    }

    , getPerimeter: function () {
        return 2 * (this.getWidth() - this.thickness + this.getHeight() - this.thickness);
    }
    
    /*
     * El gap es la diferencia entre el perimetro real y 
     * la longitud total ocupada por todos los (empalme + tramo)
     */
    , getGap: function () {
        return this.getPerimeter() - this.empalmes.length * (this.empalme_size + this.tramo_size)
    }

    /*
     * La longitud de un empalme es constante, siempre es igual al espesor de la correa
     */
    , getEmpalmeSize: function () {
        return this.thickness
    }
    
    /*
     * Para determinar la longitud de un tramo
     * Se determina el espacio que debe ser ocupado por un (empalme + tramo)
     * Luego se le resta el espesor de la correa
     */
    , getTramoSize: function () {
        return (this.getPerimeter() / this.empalmes.length) - this.thickness
    }

    /*
     * Funcion encargada de asignar una posicion a un elemento de tipo
     * empalme o tramo, además asigna las dimensiones de los tramos
     * dependiendo del borde donde es posicionado
     */
    , setPosition: function ($elem, elementType, elementSize, border) {
        // console.log('Asignando posicion...')
        if (!$elem)
            return false

        var css = {}

        if (border.sense == 'ltr') {
            css['top'] = 0
            css['right'] = 'auto'
            css['bottom'] = 'auto'
            css['left'] = border.length - border.remainder
        }
        else if (border.sense == 'rtl') {
            css['top'] = 'auto'
            css['right'] = border.length - border.remainder
            css['bottom'] = 0
            css['left'] = 'auto'
        }
        else if (border.sense == 'ttb') {
            css['top'] = border.length - border.remainder
            css['right'] = 0
            css['bottom'] = 'auto'
            css['left'] = 'auto'
        }
        else if (border.sense == 'btt') {
            css['top'] = 'auto'
            css['right'] = 'auto'
            css['bottom'] = border.length - border.remainder
            css['left'] = 0
        }

        // Se asignan las dimensiones del elemento
        if (elementType == 'tramo') {
            if (border.direction == 'horizontal') {
                css['width'] = this.getTramoSize() + 'px'
                css['height'] = this.thickness + 'px'
            }
            else if (border.direction == 'vertical') {
                css['width'] = this.thickness + 'px'
                css['height'] = this.getTramoSize() + 'px'
            }
        }
        else {
            if (border.direction == 'horizontal') {
                css['width'] = this.thickness + 'px'
                css['height'] = this.thickness + 'px'
            }
            else if (border.direction == 'vertical') {
                css['width'] = this.thickness + 'px'
                css['height'] = this.thickness + 'px'
            }
        }

        // Se actualiza lo que queda sin ocupar del borde
        border.remainder -= elementSize
        
        // Se actualizan las propiedades del elemento
        $elem.css(css)

        return true
    }

    , getCurrentBorder: function () {
        return this.borders[this.current_border]
    }

    , nextBorder: function () {
        if (this.current_border == 3) {
            return undefined
        }
        else {
            this.current_border++
            return this.borders[this.current_border]
        }
    }

    , getCurrentElement: function () {
        if (this.current_element_type == 'empalme') {
            if (this.empalmes.length) {
                return this.empalmes[this.current_empalme]
            }
        }
        else if (this.current_element_type == 'tramo') {
            if (this.tramos.length) {
                return this.tramos[this.current_tramo]
            }
        }
        return undefined
    }

    , getCurrentElementType: function () {
        return this.current_element_type
    }

    ,getCurrentElementSize: function () {
        if (this.current_element_type == 'empalme') 
            return this.getEmpalmeSize()
        
        else if (this.current_element_type == 'tramo') 
            return this.getTramoSize()
        
        return undefined
    }

    , nextElement: function () {
        if (this.current_element_type == 'empalme') {
            this.current_empalme++
            this.current_element_type = 'tramo'
        } 
        else if (this.current_element_type == 'tramo') {
            this.current_tramo++
            this.current_element_type = 'empalme'
        }

        return this.getCurrentElement()
    }

    , sort: function () {

        // Mensaje
        // console.log("Reestructurando piezas")

        // Se resetean los parametros
        this.reset()
        
        // Se debe indicar que el elemento actual
        // a ser insertado es un empalme
        this.current_element_type = 'empalme'

        // Se obtiene el primer borde (top)
        var border = this.getCurrentBorder()

        // Se obtiene el primer elemento
          , $element = this.getCurrentElement()

        // Se obtiene la longitud del primer elemento
          , elementSize = this.thickness

        // Mientras queden bordes [top, right, bottom, left]
        while (border) {

            // Cabe el elemento en el borde actual?
            // console.log("Remainder del borde " + border.sense + ": " + border.remainder)

            while (border.remainder >= elementSize) {

                // Se intenta insertar un empalme o un tramo
                this.setPosition($element, this.current_element_type, elementSize, border)

                // Se obtiene el siguiente elemento y también su longitud
                $element = this.nextElement()
                elementSize = this.getCurrentElementSize()

            }

            // Corrección para diferencias mínimas
            // Si la diferencia es muy cercana a 0, es mejor
            // insertar el elemento
            if ((Math.abs(border.remainder - elementSize) < 0.000001) &&
                (Math.abs(border.remainder - elementSize) > 0)) {

                elementSize = border.remainder

                // Se intenta insertar un empalme o un tramo
                this.setPosition($element, this.current_element_type, elementSize, border)

                // Se obtiene el siguiente elemento y también su longitud
                $element = this.nextElement()
                elementSize = this.getCurrentElementSize()
            }
                

            /*
             * Cuando un elemento no cabe significa que ha quedado un espacio
             * sin llenar, en ese caso se le setea un atributo de tipo
             * data con el ancho y altura que el elemento deberia tener.
             * Posteriormente este elemento sera redimensionado.
             *
             * Este elemento será ingresado como un cuadrado
             * si se analiza con chrome dev tools se podrá ver que parte del
             * elemento está escondido detrás de la div que ocupa el centro
             * de la correa, un truco barato.
             */
            var remainder = border.remainder
            // console.log("Remainder al terminar un borde: " + border.remainder)
            // console.log("ElemenSize al terminar un borde" + elementSize)
            if (border.remainder > 0) {
                elementSize = elementSize - border.remainder
                if ($element) {
                    $element.attr('data-remainder', 'true')
                    $element.attr('data-width', border.remainder + this.thickness)
                    $element.attr('data-height', elementSize)
                }
            }

            // Se avanza al siguiente borde
            border = this.nextBorder()
        }

        // Correccion para los elementos de las esquinas
        $('[data-remainder="true"]', this.$container).each(function() {
            $(this).width($(this).attr('data-width'))
            $(this).height($(this).attr('data-height'))

            $(this).removeAttr('data-remainder').removeAttr('data-width').removeAttr('data-height')
        })
    }

    , reset: function () {
        this.current_border = 0
        this.current_empalme = 0
        this.current_tramo = 0

        for (var i = 0; i < this.borders.length; i++) {
            if (this.borders[i].direction == 'horizontal') {
                this.borders[i].length = this.getWidth() - this.thickness
                this.borders[i].remainder = this.borders[i].length
            }
            else {
                this.borders[i].length = this.getHeight() - this.thickness
                this.borders[i].remainder = this.borders[i].length
            }
        }
    }
}


/*
 * Ready script
 */
$(document).on('ready', function () {

    // Placeholder polyfill
    $('input, textarea').placeholder()
})