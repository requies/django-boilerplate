# import hashlib
from django import template

register = template.Library()
    
@register.simple_tag
def mult(a, b):
    return a * b