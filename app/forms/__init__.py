# -*- encoding: utf-8 -*-

from app.forms.category import CategoryForm
from app.forms.story import StoryForm

# Se define cuáles son los módulos a exportar en este paquete
__all__ = [
	'CategoryForm', 
	'StoryForm'
]