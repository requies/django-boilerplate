# -*- encoding: utf-8 -*-
# Resource:
# Story story Stories stories

from django.forms import ModelForm
from app.models import Story

class StoryForm(ModelForm):
    class Meta:
        model = Story