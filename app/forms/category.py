# -*- encoding: utf-8 -*-
# Resource:
# Category category Categories categories

from django.forms import ModelForm
from app.models import Category

class CategoryForm(ModelForm):
    class Meta:
        model = Category