
# Choose the python version
PYTHON=python
# PYTHON=python2.5
# PYTHON=python2.6
# PYTHON=python2.7

BACKUP_DIR=backups

# Install dependencies
install:
	pip install `grep -v -e '^\#' -e '^\s*$$' package.pip`

# Syncdb with your app models
sync:
	$(PYTHON) manage.py syncdb

# Destruye y crea nuevamente todas las tablas
flush:
	$(PYTHON) manage.py flush

# Realiza un respaldo de todos los datos del proyecto
backup:
	mkdir -p $(BACKUP_DIR)
	$(PYTHON) manage.py dumpdata > backups/$(shell date +%s)_backup.json

# Ejecuta las pruebasu unitarias de la aplicación
test:
	$(PYTHON) manage.py test

# Ejecuta el servidor de desarrollo
run:
	$(PYTHON) manage.py runserver